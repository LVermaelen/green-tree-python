from django.http import JsonResponse
from django.http import JsonResponse
from .models import Forum
from .encoders import (
    ForumListEncoder,
    ForumCreateUpdateEncoder
)
from django.views.decorators.http import require_http_methods
import json

# Create your views here.

@require_http_methods(['GET', 'POST'])
def api_list_forums(request):
    if request.method == 'GET':
        forums = Forum.objects.all()
        return JsonResponse(
            {'forums': forums},
            encoder=ForumListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        forum = Forum.objects.create(**content)
        return JsonResponse(
            forum,
            encoder=ForumCreateUpdateEncoder,
            safe=False
        )


@require_http_methods(['GET', 'DELETE', 'PUT'])
def api_show_forum(request, slug):
    if request.method == 'GET':
        try:
            forum = Forum.objects.get(slug=slug)
            return JsonResponse(
                forum,
                encoder=ForumListEncoder,
                safe=False
            )
        except Forum.DoesNotExist:
            return JsonResponse(
                {'message': 'forum does not exist'},
                status=400,
                safe=False
            )
    elif request.method == 'DELETE':
        count, _ = Forum.objects.filter(id=pk).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        content = json.loads(request.body)
        Forum.objects.filter(id=pk).update(**content)
        forum = Forum.objects.get(id=pk)
        return JsonResponse(
            forum,
            encoder=ForumListEncoder,
            safe=False
        )
