from common.json import ModelEncoder
from .models import Forum

# Forum Encoders

class ForumListEncoder(ModelEncoder):
    model = Forum
    properties = ['slug', 'name', 'description']
    read_only_field = ['slug']

class ForumCreateUpdateEncoder(ModelEncoder):
    model = Forum
    properties = ['slug', 'name', 'description']
    read_only_fields = ('slug',)
    lookup_field = 'slug'
