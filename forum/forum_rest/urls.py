from django.urls import path
from .views import api_list_forums, api_show_forum


urlpatterns = [
    path('forums/', api_list_forums, name='api_list_attendees'),
    path('forums/<slug:slug>/', api_show_forum, name='api_show_forum')
]
