from django.http import JsonResponse
from .models import Thread
from forum_rest.models import Forum
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    ThreadListEncoder
)
# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_threads(request):
    if request.method == "GET":
        threads = Thread.objects.all()
        return JsonResponse(
            {'threads': threads},
            encoder=ThreadListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            forum = Forum.objects.get(slug=content["forum"])
            content["forum"] = forum
        except Forum.DoesNotExist:
            return JsonResponse({"message": "Invalid Forum Slug"})
        thread = Thread.objects.create(**content)
        return JsonResponse(
            thread,
            encoder=ThreadListEncoder,
            safe=False
        )
