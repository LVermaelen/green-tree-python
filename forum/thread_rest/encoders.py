from common.json import ModelEncoder
from .models import Thread

class ThreadListEncoder(ModelEncoder):
    model = Thread
    properties = [
        'id',
        'name',
        'pinned',
        'content',
        'creator',
        'created_at',
        'last_activity'
    ]

    def get_extra_data(self, o):
        return {'forum': o.forum.slug}
