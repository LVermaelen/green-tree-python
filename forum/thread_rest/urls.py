from django.urls import path
from.views import api_list_threads

urlpatterns = [
    path('threads/', api_list_threads, name='api_list_threads')
]
