from django.db import models
from django.utils.text import Truncator
from django.utils.timezone import now
from django.contrib.auth.models import User
from forum_rest.models import Forum
from django.urls import reverse

class Thread(models.Model):
    """ Model to represent a thread in a forum """
    name = models.CharField(max_length=255)
    forum = models.ForeignKey(Forum, on_delete=models.CASCADE, related_name='threads')
    pinned = models.BooleanField(default=False)
    content = models.TextField()
    image = models.ImageField(upload_to=None, height_field=100, width_field=100, max_length=None)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='threads')
    created_at = models.DateTimeField(auto_now_add=True)
    last_activity = models.DateTimeField(default=now)

    class Meta:
        ordering = [
            '-pinned',
            '-last_activity'
        ]

    def __str__(self):
        truncated_name = Truncator(self.name)
        return truncated_name.chars(30)

    def get_api_url(self):
        return reverse('api_show_thread', kwargs={'pk': self.pk})
