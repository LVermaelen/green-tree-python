from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
# Create your models here.


class UserProfile(models.Model):
    """ Model to represent additional information about user """
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='profile'
    )
    bio = models.TextField(
        max_length=2000,
        blank=True,
        default=''
    )
    # we use URL instead of imagefield because we'll use 3rd party img hosting later on
    avatar = models.URLField(default='', blank=True)
    status = models.CharField(max_length=16, default='', blank=True)
    name = models.CharField(max_length=32, default='')

    def __str__(self):
        return self.user.username

    def get_api_url(self):
        return reverse('api_show_user_profile', )
